// Sample program to show how literal, constant and variables work within
// the scope of implicit conversion.
package main

import (
	"fmt"
	"time"
)

func main() {

	// Use the time package to get the current date/time.
	now := time.Now()

	// Substract 5 nanoseconds from now using literal constant.
	literal := now.Add(-5)

	// Substract 5 seconds from now using a declared constants.
	const timeout = 5 * time.Second
	constant := now.Add(-timeout)

	// Substract 5 nanosecond from now using a variable of type int64
	// minusFive := int64(-5)
	// variable := now.Add(minusFive)

	fmt.Printf("Now		:%v\n", now)
	fmt.Printf("Literal	:%v\n", literal)
	fmt.Printf("Constant:%v\n", constant)
}
