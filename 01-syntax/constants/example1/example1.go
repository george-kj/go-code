// Sample program to show how to declare constants and their implementation in Go.
package main

func main() {

	// Constants live within the compiler.
	// They have parallel type system.
	// Compiler can perform implicit conversions of untyped constants.

	// Untyped constants.
	const ui = 12345    // kind: integer
	const uf = 3.141592 // kind: floating-point

	// Typed constants still use the constant type system but their precision
	// is restricted.
	const ti int = 12345        // type: int
	const tf float64 = 3.141592 // type: floating-point

	// Variable answer will of type float64
	var answer = 3 * 0.333 // KindFloat(3) * KindFloat(0.333)
	_ = answer

	// Constant third will be of kind floating point.
	const third = 1 / 3.0 // KindFloat(1) / KindFloat(3.0)

	// Constant zero will be of kind integer.
	const zero = 1 / 3 // KindInt(1) / KindInt(3)

	// This is an example of constant arithmetic between typed and
	// untyped constants. Must have like types to perform math.
	const one int8 = 1
	const two = 2 * one // int8(2) * int8(1)
}
