// Sample program to teach the mechanis of escape analysis.
package main

import "fmt"

// user represents a user in the system.
type user struct {
	name  string
	email string
}

func main() {
	u1 := createUserV1()
	u2 := createUserV2()

	fmt.Printf("u1: %v u2:%v\n", u1, u2)
}

// createUserV1 creates a user value and passed a copy back to the caller.
func createUserV1() user {
	u := user{
		name:  "George",
		email: "george@gmail.com",
	}

	fmt.Println("V1", u)

	return u
}

// createUserV2 creates a user value and share the address of the value.
func createUserV2() *user {
	u := user{
		name:  "George",
		email: "george@gmail.com",
	}

	fmt.Println("V1", u)

	return &u
}
