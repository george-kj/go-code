// Sample program to show the basic concepts of using a pointer to share data.

package main

import "fmt"

func main() {

	// Declare variable of type int with a value of 10.
	count := 10

	// Display the "value of" and "address of" count.
	fmt.Printf("count:\tValue Of[%v]\tAddr Of[%p]\n", count, &count)

	// Pass the "address of" count.
	increment(&count)

	fmt.Printf("count:\tValue Of[%v]\tAddr Of[%p]\n", count, &count)
}

// increment declares inc as a pointer variable whose value is always an address
// and points to values of type int.
func increment(inc *int) {

	// Increment the "value of" count that the "pointer points to"
	*inc++

	fmt.Printf("inc:\tValue Of[%v]\tAddr Of[%p]\tValue Points To[%v]\n", inc, &inc, *inc)
}
