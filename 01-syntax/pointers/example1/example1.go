// Sample program to show the basic concept of pass by value.
package main

import "fmt"

func main() {

	// Declare variable of type int with a value of 10.
	count := 10

	// Display the "value of" and "address of" count.
	fmt.Printf("Count:\tValue Of[%v]\tAddr Of[%p]\n", count, &count)

	// Pass the "value of" the count.
	increment(count)

	fmt.Printf("Count:\tValue Of[%v]\tAddr Of[%p]\n", count, &count)
}

// increment declares a variable named count and this will store values of type int.
func increment(inc int) {

	// Increment the "value of" inc.
	inc++
	fmt.Printf("inc: \tValue Of[%v]\tAddr Of[%p]\n", inc, &inc)
}
