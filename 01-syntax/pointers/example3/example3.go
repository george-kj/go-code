// Sample program to show the basic concept of using a pointer to share data.

package main

import (
	"fmt"
)

// user represents a user in the system.
type user struct {
	name   string
	email  string
	logins int
}

func main() {
	// Declare and initialize a variable named george of type user.
	george := user{
		name:  "george",
		email: "george@gmail.com",
	}

	// Pass the "address of" the george value.
	display(&george)

	// Pass the "address of" the logins fields from within the george value.
	increment(&george.logins)

	// Pass the "address of" the george value.
	display(&george)
}

// increment declares logins as a pointer variable whose value is always an
// address and points to values of type int.
func increment(logins *int) {
	*logins++
	fmt.Printf("Addr Of[%p] Value Of[%v] Value Points to[%v]\n", &logins, logins, *logins)
}

// display declares u as user pointer variable whose value is always an address and
// points to values of type user.
func display(u *user) {
	fmt.Printf("%p\t%+v\n", u, *u)
	fmt.Printf("Name: %q Email: %q Logins: Logins: %d\n", u.name, u.email, u.logins)
}
