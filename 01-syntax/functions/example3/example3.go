// Sample program to show some of the mechanics behind the short variable
// declaration operator.
package main

import "fmt"

// user is a struct type that declares user information.
type user struct {
	id   int
	name string
}

func main() {

	// Declare the error variable.
	var err1 error

	// The short variable declaration operator will declare u and redeclare err1.
	u, err1 := getUser()
	if err1 != nil {
		return
	}

	fmt.Println(u)

	// The short variable declaration operator will declare u and err2.
	u, err2 := getUser()
	if err2 != nil {
		return
	}

	fmt.Println(u)
}

// getUser returns a pointer of type user.
func getUser() (*user, error) {
	return &user{id: 1234, name: "George"}, nil
}
