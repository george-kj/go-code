// Alignment is about placing fields on address alignment boundaries for more
// efficient reads and writes to memory.

// Sample program to show how struct types align on boundaries.

package main

import (
	"fmt"
	"unsafe"
)

// No byte padding.
type nbp struct {
	a bool //	1 byte 				size 1
	b bool //	1 byte 				size 2
	c bool // 	1 byte				size 3 - Aligned on 1 byte
}

// Single byte padding.
type sbp struct {
	a bool //	1 byte				size 1
	//	  		1 byte padding		size 2
	b int16 //	2 bytes				size 4 - Aligned on 2 bytes
}

// Three byte padding.
type tbp struct {
	a bool //	1 byte				size 1
	// 			3 bytes padding		size 4
	b int32 //	4 byte 				size 8 - Aligned on 2 bytes
}

// Eight byte padding on 64bit Arch. Word size is 8 bytes.
type ebp64 struct {
	a string //	16 bytes			size 16
	b int32  //	4 bytes				size 20
	//			4 bytes padding 	size 24
	c string //	16 bytes			size 40
	d int32  //	4 bytes				size 44
	//			4 bytes padding		size 48 - Aligned on 3 bytes
}

// No padding on 32bit Arch. Word size is 4 bytes (GOARCH=386 go build).
type ebp32 struct {
	a string // 8 bytes			size 8
	b int32  // 4 bytes			size 12
	c string // 8 bytes			size 20
	d int32  //	4 bytes			size 24 - Aligned on 4 bytes
}

func main() {

	// No byte padding.
	var nbp nbp
	size := unsafe.Sizeof(nbp)
	fmt.Printf("SizeOf[%d][%p %p %p]\n", size, &nbp.a, &nbp.b, &nbp.c)

	// Single byte padding.
	var sbp sbp
	size = unsafe.Sizeof(sbp)
	fmt.Printf("SizeOf[%d][%p %p]\n", size, &sbp.a, &sbp.b)

	// Two byte padding.
	var tbp tbp
	size = unsafe.Sizeof(tbp)
	fmt.Printf("SizeOf[%d][%p %p]\n", size, &tbp.a, &tbp.b)

	// Eight byte padding.
	var ebp64 ebp64
	size = unsafe.Sizeof(ebp64)
	fmt.Printf("SizeOf[%d][%p %p %p %p]\n", size, &ebp64.a, &ebp64.b, &ebp64.c, &ebp64.d)

	// No padding.
	var ebp32 ebp32
	size = unsafe.Sizeof(ebp32)
	fmt.Printf("SizeOf[%d][%p %p %p %p]\n", size, &ebp32.a, &ebp32.b, &ebp32.c, &ebp32.d)
}
